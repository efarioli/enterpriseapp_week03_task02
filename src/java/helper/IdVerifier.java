/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

/**
 *
 * @author zeke
 */
public class IdVerifier
{
    static  public boolean validate(String id){
        boolean result = true;
        for (int i = 0; i<id.length(); i++ ){
            if(!Character.isDigit(id.charAt(i)))
                result = false;
        }
        return result;
    }
    
}
