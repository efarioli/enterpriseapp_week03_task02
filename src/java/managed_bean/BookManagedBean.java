/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed_bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author zeke
 */
@Named(value = "bookManagedBean")
@SessionScoped
public class BookManagedBean implements Serializable
{

    private String name = "";
    private List<String> contents = new ArrayList<String>();
    private String customerId = "";
    private String customerName = "";
    

    /**
     * Creates a new instance of BookManagedBean
     */
    public BookManagedBean()
    {
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String addBook(String book)
    {
        contents.add(book);
        return "Books";
    }

    public ArrayList<String> getContents()
    {
        return (ArrayList<String>) contents;
    }

    public String deleteBook(String book) throws BookDoesntExistException
    {
        boolean ex = true;
        ArrayList<String> booksCopy = new ArrayList<>();
        for (int i = 0; i < contents.size(); i++)
        {
            if (book.equals(contents.get(i)))
            {
                ex = false;
            } else
            {
                booksCopy.add(contents.get(i));
            }
        }
        try
        {
            if (ex)
            {
                throw new BookDoesntExistException("" + book + " doesn't exist");
            }
        } catch (BookDoesntExistException bex)
        {
            return "Error?faces-redirect=true";           
        }

        contents = booksCopy;

        return "Books";
    }

}
